//shadowbox
$(window).load(function(){
 var options = {
  animate: true,
  animateFade: true,
  animSequence: "sync",
  resizeDuration: 0.15,
  fadeDuration: 0.2,
  flvPlayer: 'js/flvplayer.swf',
  modal: false,
  continuous: true,
  counterType: "skip",
  counterLimit: 10,
  overlayColor: "black",
  overlayOpacity: 0.8,
  viewportPadding: 10,
  initialHeight: 250,
  initialWidth: 400
 };
 Shadowbox.init(options);
});
