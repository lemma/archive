
$(function () {
    $.nette.init();
    //jQuery.scrollSpeed(50, 400);
    new WOW().init();

    jQuery(window).load(function () {
        jQuery("#preloader").delay(100).fadeOut("slow");
    });

});


$(document).ready(function () {
    $('.fancybox').magnificPopup({type: 'image'});

});


$(document).ready(function () {
    $('.gallery-item').magnificPopup({
        type: 'image',
        gallery: {
            enabled: true
        }
    });

});

(function ($, undefined) {

    $.nette.ext({
        load: function () {
            $('[data-confirm]').click(function (event) {
                var obj = this;
                event.preventDefault();
                event.stopImmediatePropagation();
                $("<div id='dConfirm' class='modal fade'></div>").appendTo('body');
                $('#dConfirm').html("<div id='dConfirmHeader' class='modal-header'></div><div id='dConfirmBody' class='modal-body'></div><div id='dConfirmFooter' class='modal-footer'></div>");
                $('#dConfirmHeader').html("<a class='close' data-dismiss='modal'>×</a><h3 id='dConfirmTitle'></h3>");
                $('#dConfirmTitle').html($(obj).data('confirm-title'));
                $('#dConfirmBody').html($(obj).data('confirm-text'));
                $('#dConfirmFooter').html("<a id='dConfirmOk' class='btn " + $(obj).data('confirm-ok-class') + "' data-dismiss='modal'>Ano</a><a id='dConfirmCancel' class='btn " + $(obj).data('confirm-cancel-class') + "' data-dismiss='modal'>Ne</a>");
                if ($(obj).data('confirm-header-class')) {
                    $('#dConfirmHeader').addClass($(obj).data('confirm-header-class'));
                }
                if ($(obj).data('confirm-ok-text')) {
                    $('#dConfirmOk').html($(obj).data('confirm-ok-text'));
                }
                if ($(obj).data('confirm-cancel-text')) {
                    $('#dConfirmCancel').html($(obj).data('confirm-cancel-text'));
                }
                $('#dConfirmOk').on('click', function () {
                    var tagName = $(obj).prop("tagName");
                    if (tagName == 'INPUT') {
                        var form = $(obj).closest('form');
                        form.submit();
                    } else {
                        if ($(obj).data('ajax') == 'on') {
                            $.nette.ajax({
                                url: obj.href
                            });
                        } else {
                            document.location = obj.href;
                        }
                    }
                });
                $('#dConfirm').on('hidden', function () {
                    $('#dConfirm').remove();
                });
                $('#dConfirm').modal('show');
                return false;
            });
        }
    });

})(jQuery);


tinymce.init({
    height: 350,
    entity_encoding: "raw",
    forced_root_block: "",
    language: 'cs',
    mode: "textareas",
    editor_deselector: "mceNoEditor",
    plugins: [
        "advlist autolink link image lists charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
        "table contextmenu directionality emoticons paste textcolor responsivefilemanager code"
    ], link_class_list: [
        {title: 'Žádná', value: ''},
        {title: 'Popup', value: 'fancybox'},
        {title: 'Popup galerie', value: 'gallery-item'}
    ],
    toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent ",
    toolbar2: "styleselect | responsivefilemanager | image | link unlink  | forecolor  | print preview code ",
    image_advtab: true,
    relative_urls: false,
    style_formats: [
        {title: "Nadpisy", items: [
                {title: "Nadpis 1", format: "h1"},
                {title: "Nadpis 2", format: "h2"},
                {title: "Nadpis 3", format: "h3"},
                {title: "Nadpis 4", format: "h4"},
                {title: "Nadpis 5", format: "h5"},
                {title: "Nadpis 6", format: "h6"}
            ]},
        {title: "Styly", items: [
                {title: "Tučné", icon: "bold", format: "bold"},
                {title: "Kurzíva", icon: "italic", format: "italic"},
                {title: "Podtržené", icon: "underline", format: "underline"}
            ]},
        {title: "Bloky", items: [
                {title: "Odstavec", format: "p"},
                {title: "Citace", format: "blockquote"},
                {title: "Div", format: "div"}
            ]},
        {title: "Zarovnání", items: [
                {title: "Doleva", icon: "alignleft", format: "alignleft"},
                {title: "Na střed", icon: "aligncenter", format: "aligncenter"},
                {title: "Doprava", icon: "alignright", format: "alignright"},
                {title: "Do bloku", icon: "alignjustify", format: "alignjustify"}
            ]},
        {title: "Obrázek", items: [
                {
                    title: 'Zarovnat doleva',
                    selector: 'img',
                    styles: {
                        'float': 'left',
                        'margin': '0 20px 0 0'
                    }
                },
                {
                    title: 'Zarovnat doprava',
                    selector: 'img',
                    styles: {
                        'float': 'right',
                        'margin': '0 0 0 20px'
                    }
                },
                {
                    title: 'Ohraničení 10px',
                    selector: 'img',
                    styles: {
                        'margin': '10px'
                    }
                },
                {
                    title: 'Ohraničení 20px',
                    selector: 'img',
                    styles: {
                        'margin': '20px'
                    }
                }
            ]}
    ],
    external_filemanager_path: "http://lemma.ics.muni.cz/pater/fffi/www/tinymce/js/tinymce/filemanager/",
    filemanager_title: "Prohlížeč souborů",
    external_plugins: {"filemanager": "filemanager/plugin.min.js"}
});



