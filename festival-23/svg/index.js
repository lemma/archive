export { default as Logo } from './logo_kratke.inline.svg';

export { default as LogoIg } from './socials/ig.inline.svg';
export { default as LogoFb } from './socials/fb.inline.svg';
export { default as LogoYt } from './socials/yt.inline.svg';

export { default as ProgramTimelineCs } from './timelines/program-cs.inline.svg';
export { default as ProgramTimelineEn } from './timelines/program-en.inline.svg';

export { default as TriangleFull } from './shapes/triangle-full.inline.svg';
export { default as TriangleHollow } from './shapes/triangle-hollow.inline.svg';
